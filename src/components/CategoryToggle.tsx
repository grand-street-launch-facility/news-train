import React, { useCallback, FunctionComponent, Fragment } from 'react';
import { Switch, FormControlLabel,Typography} from '@material-ui/core';
import { useCategories } from '../custom-hooks/useCategories';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const CategoryToggle: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { categories, setCategories } = useCategories();

  const setCategoriesCallback = useCallback(() => {
    const newCategory = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(categories)))
            .filter((category: [string, unknown]) => category[0] === props.text)
            .map((category: [string, unknown]) => {
              return [
                category[0],
                {
                  ...fromEntries(
                    entries({
                      ...(category[1] as Record<string, unknown>),
                    })
                      .filter(
                        (attribute: [string, unknown]) =>
                          attribute[0] === 'checked'
                      )
                      .map((attribute : [string, unknown]) => [attribute[0], !attribute[1]])
                  ),
                  ...fromEntries(
                    entries({
                      ...(category[1] as Record<string, unknown>),
                    }).filter((attribute : [string, unknown]) => attribute[0] !== 'checked')
                  ),
                },
              ];
            })
        ),
      })
    );
    setCategories({ ...JSON.parse(JSON.stringify(categories)), ...newCategory });
  }, [categories, props.text, setCategories]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(categories)))
        .filter((category : [string, unknown]) => category[0] === props.text)
        .map((category : [string, unknown]) => {
          const attributes = category[1] as Record<string, unknown>;
          return (
            <FormControlLabel
              key={category[0]}
              control={
                <Switch
                  size='medium'
                  checked={Object.values(
                    fromEntries(
                      [entries({...attributes})].flat().filter(
                        (attribute : [string, unknown]) => attribute[0] === 'checked'
                      )
                    )
                  ).some(checked => checked)}
                  onChange={() => setCategoriesCallback()}
                  name={props.text}
                />
              }
              label={<Typography variant="h3">{props.text}</Typography>}
            />
          );
        })}
    </Fragment>
  );
};

export default CategoryToggle;
