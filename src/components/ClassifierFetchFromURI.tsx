import { CategoryContext } from './Categories';
import { useURIs } from '../custom-hooks/useURIs';
import React, {
  FunctionComponent,
  useCallback,
  createContext,
  useEffect,
  useState,
  useContext
} from 'react';

import { useCorsProxies } from '../custom-hooks/useCorsProxies';
import { fetchViaProxy, fetchMultiViaProxy} from '..'
// import { URIContext } from './URIFetch'

export const FetchedClassifierContext = createContext({});

const ClassifierFetchFromURI: FunctionComponent = ({ children }) => {

  const [fetchedClassifier, setFetchedClassifier] = useState({});

  const setFetchedClassifiercallback = useCallback(
    (content: object) => {
      const newcontent = { ...content };
      setFetchedClassifier(newcontent);
    },
    [setFetchedClassifier]
  );

  const { corsProxies } = useCorsProxies();
  
  // const uriContext = useContext(URIContext);
  // const uris: any = uriContext

  // useEffect(() => {
  //   const checkedCorsProxies = Object.entries(corsProxies)
  //     .filter(
  //       corsproxyentry =>
  //         Object.assign(corsproxyentry[1] as Object).checked === true
  //     )
  //     .map(corsproxyentry => corsproxyentry[0])
  //     .filter(noblanks => !!noblanks);
  //
  //   fetchMultiViaProxy(dispatchers, checkedCorsProxies)
  //   .then((fetchedClassifiers: object) => {
  //     console.log(classifierDispatchers)
  //     console.log(fetchedClassifiers)
  //     // decrypt here
  //     // setfetchedClassifiercallback(fetchedClassifier)
  //     }
  //   )
  // }, [corsProxies, classifierDispatchers, setfetchedClassifiercallback])

  // const fetchedClassifierObj = JSON.parse(JSON.stringify(fetchedClassifier))
  return <>{children}</>
  //return (
  //  <FetchedClassifierContext.Provider value={fetchedClassifierobj}>
  //    <>{children}</>
   // </FetchedClassifierContext.Provider>
  //);
};

export default ClassifierFetchFromURI;

//
// const classifierfetchfromuri: functioncomponent = ({ children }) => {
//   const categorycontext = usecontext(categorycontext);
//   const category = categorycontext.tostring();
//   const blockstackstoragecontext = usecontext(blockstackstoragecontext);
//   const blockstackstorage = Object.assign(blockstackstoragecontext);
//   const {classifiers} = useclassifiers()
//   const {uris} = useuris()
//
//   const fetcher = (category: string, blockstackstorage: any) => {
//     return new promise(resolve => {
//       //resolve({})
//       // blockstackstorage.getfile(`classifiers_${category}`, {
//       //   decrypt: true,
//       // })
//       // .then((fetchedClassifier: string) => {
//       //   var newclassifier = bayes.fromJSON.fetchedClassifier)
//       //   resolve(newclassifier)
//       // })
//       // .catch((error: Object. => {
//       //   const deepcopyclassifiers: { [key: string]: Object.} = classifiers
//       //   let newclassifier
//       //   try {
//       //     newclassifier = !!deepcopyclassifiers[category] ? bayes.fromJSON.`${JSON.stringify(deepcopyclassifiers[category])}`) : bayes()
//       //   } catch (err) {
//       //     newclassifier = bayes()
//       //   }
//       //   resolve(newclassifier)
//       //})
//     });
//   };
//
//   const { data } = useswr([category, blockstackstorage], fetcher, {
//     suspense: true,
//     shouldretryonerror: true,
//     dedupinginterval: 60 * 1000,
//     revalidateonfocus: false
//   });
//
//   const classifier: Object.= Object.assign(
//     data as record<string, unknown>
//   );
//
//   return (
//     <classifierContext.Provider value={classifier}>
//       {children}
//     </classifierContext.Provider>
//   );
// };
//
// classifierfetchfromuri.proptypes = {
//   children: proptypes.node.isrequired,
// };
//
// export default ClassifierFetchFromURI;
