import React, {
  FunctionComponent,
  useState,
  useCallback,
} from 'react';

import { TextField } from '@material-ui/core';
import { useFeeds } from '../custom-hooks/useFeeds';

const FeedsAdd: FunctionComponent = () => {
  const [inputValue, setInputValue] = useState('');
  const { feeds, setFeeds } = useFeeds();

  const setInputCallback = useCallback(
    (newInputValue: string) => {
      setInputValue(newInputValue);
    },
    [setInputValue]
  );

  const addFeedCallback = useCallback(() => {
    const newFeed = JSON.parse(
      `{"${inputValue}": {"checked": true, "categories": []}}`
    );
    setFeeds({ ...newFeed, ...JSON.parse(JSON.stringify(feeds)) });
  }, [feeds, setFeeds, inputValue]);

  return (
    <>
      <TextField
        id="addFeedTextField"
        placeholder="add feed here"
        value={inputValue}
        onKeyPress={event => {
          [event.key]
            .filter(theKey => theKey === 'Enter')
            .map(() => {
              addFeedCallback();
              setInputCallback('');
              return 'o';
            });
        }}
        onChange={event => {
          setInputCallback(event.target.value);
        }}
        fullWidth
      />
    </>
  );
};

export default FeedsAdd;
