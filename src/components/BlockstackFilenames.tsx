import React, { FunctionComponent, Suspense} from 'react';
import { RouteComponentProps } from  '@reach/router';
import BlockstackFilenamesFetch from './BlockstackFilenamesFetch'
import BlockstackFilenamesDisplay from './BlockstackFilenamesDisplay'

const BlockstackFilenames: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  return (
    <Suspense fallback={<>fetching blockstack filenames</>}>
      <BlockstackFilenamesFetch>
        <BlockstackFilenamesDisplay />
      </BlockstackFilenamesFetch>
    </Suspense>
  )
}

export default BlockstackFilenames;
