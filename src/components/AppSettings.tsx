import React, { FunctionComponent } from 'react';
import { Box } from '@material-ui/core';

import AppSettingsToggle from './AppSettingsToggle';
import AppSettingsReset from './AppSettingsReset';
import {useSettings} from '../custom-hooks/useSettings'

const Settings: FunctionComponent = () => {
  const {settings} = useSettings()
  return (
    <>
      <Box p={1}>
        {Object.keys(settings as object).map(appSetting => {
          return <AppSettingsToggle name={appSetting} key={appSetting} />;
        })}
      </Box>
      <Box p={1}>
        <AppSettingsReset />
      </Box>
    </>
  );
};

export default Settings;
