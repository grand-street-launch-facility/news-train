import React, { useCallback, FunctionComponent, Fragment } from 'react';
import { Switch, FormControlLabel } from '@material-ui/core';
import { useCorsProxies } from '../custom-hooks/useCorsProxies';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const CorsProxyToggle: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { corsProxies, setCorsProxies } = useCorsProxies();

  const setCorsProxiesCallback = useCallback(() => {
    console.log(corsProxies);
    const newCorsProxy = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(corsProxies)))
            .filter((corsProxy: [string, unknown]) => corsProxy[0] === props.text)
            .map((corsProxy: [string, unknown]) => {
              return [
                corsProxy[0],
                {
                  ...fromEntries(
                    entries({
                      ...(corsProxy[1] as Record<string, unknown>),
                    })
                      .filter(
                        (attribute: [string, unknown]) =>
                          attribute[0] === 'checked'
                      )
                      .map((attribute: [string, unknown]) => [attribute[0], !attribute[1]])
                  ),
                  ...fromEntries(
                    entries({
                      ...(corsProxy[1] as Record<string, unknown>),
                    }).filter((attribute: [string, unknown]) => attribute[0] !== 'checked')
                  ),
                },
              ];
            })
        ),
      })
    );
    setCorsProxies({ ...JSON.parse(JSON.stringify(corsProxies)), ...newCorsProxy });
  }, [corsProxies, props.text, setCorsProxies]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(corsProxies)))
        .filter((corsProxy: [string, unknown]) => corsProxy[0] === props.text)
        .map((corsProxy: [string, unknown]) => {
          const attributes = corsProxy[1] as Record<string, unknown>;
          return (
            <FormControlLabel
              key={corsProxy[0]}
              control={
                <Switch
                  checked={Object.values(
                    fromEntries(
                      entries(attributes).filter(
                        (attribute: [string, unknown]) => attribute[0] === 'checked'
                      )
                    )
                  ).some(checked => checked)}
                  onChange={() => setCorsProxiesCallback()}
                  name={props.text}
                />
              }
              label={props.text}
            />
          );
        })}
    </Fragment>
  );
};

export default CorsProxyToggle;
