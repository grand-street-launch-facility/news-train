import React, {
  useContext,
  createContext,
  FunctionComponent,
  useState,
  useCallback,
  useEffect,
} from 'react';

import xml2js from 'xml2js';
import { FetchedContentContext } from './FeedContentFetch';
var fromEntries = require('fromentries')
var entries = require('object.entries')

export const ParsedFeedContentContext = createContext({});

const parseFeedContentMulti = (fetchedContent: object) => {
  return new Promise((resolve, reject) => {
    const parseFeedQueue: object[] = [];
    [entries(fetchedContent)]
      .flat()
      .filter(fetchedContentEntry => fetchedContentEntry[1] !== undefined)
      .forEach((fetchedContentEntry: [string, object]) => {
        parseFeedQueue.push(
          new Promise((resolve, reject) => {
            const parser = new xml2js.Parser();
            parser.parseString(
              Object.assign({ ...fetchedContentEntry[1] } as object).data,
              function (err: object, result: {rss?: {channel?: {item: object[]}}, feed?: {entry?: object} }) {
                if (err) {
                  return;
                }
                const channelContentItem = [result.rss?.channel]
                  .flat()
                  .filter(channel => !!channel?.item)
                  .map(channel => {
                    return channel?.item
                    .filter(item => item !== {})
                    .map((item: {title?: string, link?: string, description?: string}) => {
                      return {
                        title: [item.title].flat().find(() => true),
                        link: [item.link].flat().find(() => true),
                        description: [item.description].flat().find(() => true),
                      }
                    })
                  })

                // atom feeds have entry instead of item
                const channelContentEntry = [result.feed]
                  .flat()
                  .filter(feed => !!feed?.entry)
                  .map(feed => [feed?.entry].flat().map((feedEntry?: object) => {
                    const rawLink: any = [entries(feedEntry).find((attribute: [string, unknown]) => attribute[0] === 'link')].flat().slice(-1).find(() => true)
                    const linkVal: any = [Object.values(rawLink)].flat().find(() => true)
                    const linkObj: any = [Object.values(linkVal)].flat().find(() => true)
                    const hrefEntry: any[] = entries(linkObj).filter((attribute: [string, unknown]) => attribute[0] === 'href')
                    const hrefVal: unknown = Object.values(fromEntries(hrefEntry)).find(() => true)
                    const rawTitle : any = [entries(feedEntry).find((attribute: [string, unknown]) => attribute[0] === 'title')].flat().slice(-1).find(() => true)
                    const titleVal : any = [Object.values(rawTitle)].flat().find(() => true)
                    const titleStr: any = [Object.values(titleVal)].flat().find(() => true)
                    const rawDescription: any = [entries(feedEntry).find((attribute: [string, unknown]) => attribute[0] === 'summary')].flat().slice(-1).find(() => true)
                    const descriptionVal : any = [Object.values({...rawDescription})].flat().find(() => true)
                    const descriptionStr: any = [[Object.values(descriptionVal)].flat().find(() => true)].slice(-1).find(() => true)

                    return {
                      title: `${titleStr}`,
                      link: `${hrefVal}`,
                      description: `${descriptionStr}`,
                    }
                  })
                  )
                resolve([
                  fetchedContentEntry[0],
                  {
                    ...[...channelContentItem, ...channelContentEntry]
                  },
                ]);
              }
            );
          })
        );
      });
    Promise.all(parseFeedQueue).then((parsedContent: object) => {
      resolve(fromEntries(parsedContent));
    });
  });
};

const FeedContentParse: FunctionComponent = ({ children }) => {
  const fetchedContentContext = useContext(FetchedContentContext);
  const fetchedContent = Object.assign(fetchedContentContext);
  const [parsedFeedContent, setParsedFeedContent] = useState({});
  const setParsedFeedContentCallback = useCallback(
    (parsedContent: any) => {
      setParsedFeedContent(parsedContent);
    },
    [setParsedFeedContent]
  );

  useEffect(() => {
      parseFeedContentMulti(fetchedContent)
      .then((parsedFeedContent: any) => {
        setParsedFeedContentCallback(parsedFeedContent);
      })
      .catch((error: any) => console.log(error))
  }, [setParsedFeedContentCallback, fetchedContent]);

  return (
    <ParsedFeedContentContext.Provider value={parsedFeedContent}>
      <>{children}</>
    </ParsedFeedContentContext.Provider>
  );
};

export default FeedContentParse;
