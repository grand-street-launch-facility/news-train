import React, { useCallback, FunctionComponent, Fragment } from 'react';
import { Chip } from '@material-ui/core';
import { useFeeds } from '../custom-hooks/useFeeds';
import { useCategories } from '../custom-hooks/useCategories';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const FeedCategories: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { feeds, setFeeds } = useFeeds();
  const { categories } = useCategories();

  const setFeedsCallback = useCallback(
    newFeeds => {
      setFeeds(newFeeds);
    },
    [setFeeds]
  );

  const appendOrRemoveChip = (category: string) => {
    setFeedsCallback({
      ...JSON.parse(JSON.stringify(feeds)),
      ...fromEntries(
        entries(JSON.parse(JSON.stringify(feeds)))
          .filter((feed: [string, unknown]) => feed[0] !== props.text)
          .concat(
            entries(JSON.parse(JSON.stringify(feeds)))
              .filter((feed: [string, unknown]) => feed[0] === props.text)
              .map((feed: [string, unknown]) => {
                return [
                  feed[0],
                  {
                    ...JSON.parse(JSON.stringify(feed[1])),
                    categories: [
                      ...entries(JSON.parse(JSON.stringify(feeds)))
                        .filter((feed: [string, unknown]) => feed[0] === props.text)
                        .map((feed: [string, unknown]) => {
                          const attributes = feed[1] as Record<string, unknown>;
                          const feedCategories: string[] = JSON.parse(
                            JSON.stringify(attributes['categories'])
                          );
                          return feedCategories;
                        })
                        .flat()
                        .filter((removeCategory: string) => removeCategory !== category),
                      ...[
                        entries(JSON.parse(JSON.stringify(feeds)))
                          .filter((feed: [string, unknown]) => feed[0] === props.text)
                          .map((feed: [string, unknown]) => {
                            const attributes = feed[1] as Record<
                              string,
                              unknown
                            >;
                            const feedCategories: string[] = JSON.parse(
                              JSON.stringify(attributes['categories'])
                            );
                            return feedCategories;
                          })
                          .flat()
                          .concat(category)
                          .every((e: object, i: number, a: object[]) => a.indexOf(e) === i),
                      ]
                        .filter(addCategory => addCategory)
                        .map(() => category),
                    ],
                  },
                ];
              })
          )
      ),
    });
  };

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(feeds)))
        .filter((feed: [string, unknown]) => feed[0] === props.text)
        .map((feed: [string, unknown]) => {
          const attributes = feed[1] as Record<string, unknown>;
          const feedCategories: string[] = JSON.parse(
            JSON.stringify(attributes['categories'])
          );
          return (
            <Fragment key={feed[0]}>
              {entries(JSON.parse(JSON.stringify(categories))).map((category: [string, unknown]) => {
                return [
                  feedCategories
                    .filter(feedCategory => {
                      return feedCategory === category[0];
                    })
                    .map(() => (
                      <Chip
                        key={category[0]}
                        label={category[0]}
                        color={'primary'}
                        onClick={() => appendOrRemoveChip(category[0])}
                      />
                    )),
                  <Chip
                    key={category[0]}
                    label={category[0]}
                    color={'default'}
                    onClick={() => appendOrRemoveChip(category[0])}
                  />,
                ]
                  .flat()
                  .find(() => true);
              })}
            </Fragment>
          );
        })}
    </Fragment>
  );
};
export default FeedCategories;
