import React, { FunctionComponent,Fragment } from 'react';
import { Box, Link, Divider, Typography } from '@material-ui/core';
import { RouteComponentProps } from '@reach/router';
import NavBar from './NavBar'
import { CssBaseline, Container } from '@mui/material';

const About: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  return (
    <Fragment>
     <CssBaseline />
     <Container maxWidth="sm">
      <Box sx={{display: 'flex', flexDirection: 'column'}}>
      <NavBar>
        <Link href="/news">news</Link>
        <Link href="/settings">settings</Link>
        <Link href="/classifiers">classifiers</Link>
      </NavBar>
      <Divider />
      <Box>
        <Typography variant="h1">supression:</Typography>
      </Box>
      <Box sx={{display: 'flex', flexDirection: 'column'}}>
        <Box>
        use suppression as a tool to drill through the incessant fluff in high frequency news sites to make way for what is actually important.
        </Box>
        <Box>
          <Typography variant="h1">email: </Typography>
        </Box>
        <Box>
          <Typography variant="h5">cole@cafe-society.news</Typography>
        </Box>
      </Box>
      </Box>
      <Box>
      <Box>
        <Typography variant="h1">source code:{' '}</Typography>
        <Link href="https://gitlab.com/cole.albon/news-train">
          <Typography variant="h5">https://gitlab.com/cole.albon/news-train</Typography>
        </Link>
      </Box>
      <Box><Typography variant="h1">bitcoin: </Typography>
          <Link href="https://www.blockchain.com/btc/address/33nkpL1ANUU7kAv27be6FM4BA6RsS4ZegH"><Typography variant="h5">33nkpL1ANUU7kAv27be6FM4BA6RsS4ZegH</Typography></Link>
        </Box>
        <Box>
          <Typography variant="h1">STX: </Typography>
          <Link href="https://explorer.stacks.co/address/SP2A82Q7YZJBKKT6BHD5JXPVZZ9WDRA9AAFTNZGE1?chain=mainnet"><Typography variant="h5">SP2A82Q7YZJBKKT6BHD5JXPVZZ9WDRA9AAFTNZGE1</Typography></Link>
        </Box>
        <Divider />
        <Box display="none">{props.path}</Box>
      </Box>
      </Container>
      </Fragment>
  );
};

export default About;
