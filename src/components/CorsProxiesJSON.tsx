import React, { FunctionComponent } from 'react';

import { useCorsProxies } from '../custom-hooks/useCorsProxies';

const CorsProxiesJSON: FunctionComponent = () => {
  const { corsProxies } = useCorsProxies();
  return <pre>{JSON.stringify(corsProxies, null, 2)}</pre>;
};

export default CorsProxiesJSON;
