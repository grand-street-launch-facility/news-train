import React, { useCallback, FunctionComponent, Fragment } from 'react';
import { Switch, FormControlLabel } from '@material-ui/core';
import { useFeeds } from '../custom-hooks/useFeeds';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const FeedToggle: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { feeds, setFeeds } = useFeeds();

  const setFeedsCallback = useCallback(() => {
    const newFeed = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(feeds)))
            .filter((feed: [string, unknown]) => feed[0] === props.text)
            .map((feed: [string, unknown]) => {
              return [
                feed[0],
                {
                  ...fromEntries(
                    entries({
                      ...(feed[1] as Record<string, unknown>),
                    })
                      .filter(
                        (attribute: [string, unknown]) =>
                          attribute[0] === 'checked'
                      )
                      .map((attribute: [string, unknown]) => [attribute[0], !attribute[1]])
                  ),
                  ...fromEntries(
                    entries({
                      ...(feed[1] as Record<string, unknown>),
                    }).filter((attribute: [string, unknown]) => attribute[0] !== 'checked')
                  ),
                },
              ];
            })
        ),
      })
    );
    setFeeds({ ...JSON.parse(JSON.stringify(feeds)), ...newFeed });
  }, [feeds, props.text, setFeeds]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(feeds)))
        .filter((feed: [string, unknown]) => feed[0] === props.text)
        .map((feed: [string, unknown]) => {
          const attributes = feed[1] as Record<string, unknown>;
          return (
            <FormControlLabel
              key={feed[0]}
              control={
                <Switch
                  checked={Object.values(
                    fromEntries(
                      entries(attributes).filter(
                        (attribute: [string, unknown]) => attribute[0] === 'checked'
                      )
                    )
                  ).some(checked => checked)}
                  onChange={() => setFeedsCallback()}
                  name={props.text}
                />
              }
              label={props.text}
            />
          );
        })}
    </Fragment>
  );
};

export default FeedToggle;
