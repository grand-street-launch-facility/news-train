import React, {
  useCallback,
  FunctionComponent,
  Fragment,
} from 'react';
import { IconButton } from '@material-ui/core';
import DeleteOutlined from '@material-ui/icons/DeleteOutlined';
import { useKeys } from '../custom-hooks/useKeys';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const KeyDelete: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { keys, setKeys } = useKeys();
  const deleteKey = useCallback(() => {
    const newKeys = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(keys))).filter((keyEntry: [string, unknown]) => keyEntry[0] !== props.text)
        ),
      })
    );
    setKeys(newKeys);
  }, [keys, props.text, setKeys]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(keys)))
        .filter((keyEntry: [string, unknown]) => keyEntry[0] === props.text)
        .map((keyEntry: [string, unknown]) => {
          return (
            <Fragment key={`${keyEntry[0]}`}>
              <IconButton aria-label="Delete Key" onClick={deleteKey}>
                <DeleteOutlined />
              </IconButton>
            </Fragment>
          );
        })}
    </Fragment>
  );
};

export default KeyDelete;
