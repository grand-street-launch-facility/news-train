import React, { FunctionComponent } from 'react';
import PropTypes from 'prop-types';
import { useCorsProxies } from '../custom-hooks/useCorsProxies';

export const CorsProxiesContext = React.createContext({});

const CorsProxiesProvider: FunctionComponent = ({ children }) => {
  const { corsProxies } = useCorsProxies();
  const deepCopyCorsProxies: Record<string, unknown> = { ...JSON.parse(JSON.stringify(corsProxies)) };
  return (
    <CorsProxiesContext.Provider value={deepCopyCorsProxies}>
      {children}
    </CorsProxiesContext.Provider>
  );
};

CorsProxiesProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CorsProxiesProvider;
