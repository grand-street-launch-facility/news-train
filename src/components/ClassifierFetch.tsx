import React, {
  FunctionComponent,
  useContext,
  useCallback
} from 'react';
import PropTypes from 'prop-types';
import { BlockstackStorageContext } from './BlockstackSessionProvider';
import { CategoryContext } from './Categories';
import {useClassifiers} from '../custom-hooks/useClassifiers';
import useSWR from 'swr';
import { BlockstackFilenamesContext } from './BlockstackFilenamesFetch';

var fromEntries = require('fromentries')
var entries = require('object.entries')
const bayes = require('classificator');

export const ClassifierContext = React.createContext({});

const ClassifierFetch: FunctionComponent = ({ children }) => {
  const categoryContext = useContext(CategoryContext);
  const category = categoryContext.toString();
  const blockstackStorageContext = useContext(BlockstackStorageContext);
  const blockstackStorage = Object.assign(blockstackStorageContext);
  const blockstackFilenamesContext = useContext(BlockstackFilenamesContext);
  const blockstackFilenames = { ...blockstackFilenamesContext };

  const {classifiers, setClassifiers} = useClassifiers()

  const setClassifiersCallback = useCallback((newClassifiers) => {
    const newClassifiersObj = Object.assign(newClassifiers as object)
    setClassifiers(newClassifiersObj)
  }, [setClassifiers])

  const fetcher = (category: string, blockstackStorage: {getFile: Function}) => {
    return new Promise(resolve => {
      const fetchQueue: object[] = []
      Object.values(blockstackFilenames).filter((filename: unknown) => {
        return filename === `classifiers_${category}`
      }).forEach((filename: unknown) => {
        fetchQueue.push(
          //blockstackStorage.deleteFile(`classifiers_${category}`)
          blockstackStorage.getFile(`classifiers_${category}`, {
            decrypt: true
          })
          .then((content: object) => {
            var newClassifier = bayes.fromJson(content)
            setClassifiersCallback(
              fromEntries(entries(classifiers)
              .filter((classifierWithCategory: [string, object]) => classifierWithCategory[0] !== category)
              .concat([`${category}`, newClassifier]))
            )
            resolve(newClassifier)
          })
          .catch((error: object) => {
            const deepCopyClassifiers: { [key: string]: object } = JSON.parse(JSON.stringify(classifiers))
            let newClassifier
            try {
              newClassifier = !!deepCopyClassifiers[category] ? bayes.fromJson(`${JSON.stringify(deepCopyClassifiers[category])}`) : bayes()
            } catch (err) {
              newClassifier = bayes()
            }
            resolve(newClassifier)
          })
        )
      })
      Promise.all(fetchQueue)
      .catch((error) => console.log(error))
      .finally(() => {
        let newClassifier = bayes()
        resolve(newClassifier)
      })
    })
  }

  const { data, error } = useSWR([category, blockstackStorage], fetcher, {
    suspense: true,
    shouldRetryOnError: false,
    revalidateOnFocus: false
  });

  if (!!error) {
    let newClassifier
    newClassifier = bayes()
    const classifier = Object.assign(newClassifier)
    return (
      <ClassifierContext.Provider value={classifier}>
        {children}
      </ClassifierContext.Provider>
    )
  }

  const classifier: object = Object.assign(
    data as Record<string, unknown>
  );
  return (
    <ClassifierContext.Provider value={classifier}>
      {children}
    </ClassifierContext.Provider>
  );
};

ClassifierFetch.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ClassifierFetch;
