import React, {
  FunctionComponent,
  useContext,
} from 'react';
import PropTypes from 'prop-types';
import { BlockstackStorageContext } from './BlockstackSessionProvider';
import useSWR from 'swr';

export const BlockstackFilenamesContext = React.createContext({});

const BlockstackFilenamesFetch: FunctionComponent = ({ children }) => {
  const blockstackStorageContext = useContext(BlockstackStorageContext);
  const blockstackStorage = Object.assign(blockstackStorageContext);
  const fetcher = () => {
    return new Promise((resolve) => {
      const fetchedFilenames: string[] = []
      blockstackStorage.listFiles((filename: string) => {
        fetchedFilenames.push(filename)
        return true
      })
      .then(() => {
        resolve(fetchedFilenames)
      })
      .catch(() => resolve([]))
    })
  }

  const { data, error } = useSWR('blockstackFilenames', fetcher, {
    suspense: true,
    shouldRetryOnError: false,
    revalidateOnFocus: false
  });

  if (!!error) {
    return <>{children}</>
  }

  const blockstackFilenames: string[] = Object.values(data as object)

  return (
    <BlockstackFilenamesContext.Provider value={blockstackFilenames}>
      {children}
    </BlockstackFilenamesContext.Provider>
  );
};

BlockstackFilenamesFetch.propTypes = {
  children: PropTypes.node.isRequired,
};

export default BlockstackFilenamesFetch;
