import React, {
  createContext,
  useContext,
  useEffect,
  useState,
  useCallback,
  FunctionComponent,
} from 'react';
import htmlToText from 'html-to-text';
import { ClassifierContext } from './ClassifierFetch';
import { CategoryContext } from './Categories';
import { PostContext} from './Posts'
import { removePunctuation } from '../index.js'
export const BayesPredictionContext = createContext({});

const ApplyBayesPredictions: FunctionComponent = ({ children }) => {
  const categoryContext = useContext(CategoryContext);
  const category = JSON.parse(JSON.stringify(categoryContext));
  const [bayesPrediction, setBayesPrediction] = useState({});
  const classifierContext = useContext(ClassifierContext);
  const classifier = Object.assign(classifierContext);
  const postContext = useContext(PostContext);
  const post = Object.assign(postContext);

  const setBayesPredictionCallback = useCallback(newBayesPrediction => {
    setBayesPrediction(newBayesPrediction);
  }, [setBayesPrediction]);

  useEffect(() => {
    const theDescription = `${post.description}`
    const descriptionText = htmlToText.fromString(theDescription, {
      ignoreHref: true,
      ignoreImage: true,
    });
    const theTitle = `${post.title}`
    const mlText =  removePunctuation(`${theTitle} ${descriptionText}`)
    try {
      classifier.learn(mlText, 'good');
      classifier.unlearn(mlText, 'good');
      classifier.learn(mlText, 'notgood');
      classifier.unlearn(mlText, 'notgood');
      const prediction = classifier.categorize(`${mlText}`);
      setBayesPredictionCallback(prediction)
    } catch {
      setBayesPredictionCallback({})
    }

  }, [
      setBayesPredictionCallback,
      category,
      classifier,
      post.description,
      post.title
    ]);

  const bayesPredictionObj = Object.assign({...bayesPrediction});
  return (
    <BayesPredictionContext.Provider value={bayesPredictionObj}>
      <>{children}</>
    </BayesPredictionContext.Provider>
  );
};

export default ApplyBayesPredictions;
