import React, { FunctionComponent } from 'react';
import Button from '@material-ui/core/Button';
import { useSettings } from '../custom-hooks/useSettings';

const AppSettingsReset: FunctionComponent = () => {
  const { factoryReset } = useSettings();
  return (
    <Button key="appsettingsreset" onClick={() => factoryReset()}>
      reset app settings
    </Button>
  );
};
export default AppSettingsReset;
