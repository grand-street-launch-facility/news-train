import React, { FunctionComponent, Suspense, Fragment } from 'react';
import { Router } from '@reach/router';
import { CssBaseline } from '@mui/material';
import Classifiers from './Classifiers'
import News from './News';
import Settings from './Settings';
import About from './About';
import Keys from './Keys';
import ProcessedPosts from './ProcessedPosts'
import BlockstackSessionProvider from './BlockstackSessionProvider';
import BlockstackFilenames from './BlockstackFilenames'
import Payment from './Payment';

const App: FunctionComponent = () => {
  return (
    <Fragment>
      <CssBaseline />
      <Suspense fallback={<>loading blockstack...</>}>
        <BlockstackSessionProvider>
          <Router>
            <News path="/" />
            <News path="/news" />
            <About path="/about" />
            <Settings path="/settings" />
            <Classifiers path="/classifiers" />
            <Keys path="/keys" />
            <ProcessedPosts path="/processed" />
            <BlockstackFilenames path="/blockstackfilenames" />
            <Payment path="/payment" />
          </Router>
        </BlockstackSessionProvider>
      </Suspense>
    </Fragment>
  )
};

export default App;
