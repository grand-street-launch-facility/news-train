import React, {
  FunctionComponent,
} from 'react';
import { RouteComponentProps } from '@reach/router';

import ProcessedPostsFetch from './ProcessedPostsFetch'

const ProcessedPosts: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  // const { categories } = useCategories();
  // const { feeds } = useFeeds();
  //
  // const checkedCategories = Object.entries(JSON.parse(JSON.stringify(categories)))
  //   .filter(category => Object.assign(category[1] as object).checked === true)
  //   .map(category => category[0]);
  //
  // const feedsForCategory = ((category: string) => {
  //   const theFeeds = Object.entries(JSON.parse(JSON.stringify(feeds)))
  //     .filter(feedEntry => {
  //       return Object.entries(JSON.parse(JSON.stringify(feedEntry[1])))
  //         .filter(feedEntryAttribute => {
  //           return feedEntryAttribute[0] === 'categories';
  //         })
  //         .find(feedEntryAttribute => {
  //           return (
  //             [feedEntryAttribute[1]]
  //               .flat()
  //               .indexOf(category) !== -1
  //           );
  //         })
  //     })
  //     .filter(feedEntry => {
  //       return Object.entries(JSON.parse(JSON.stringify(feedEntry[1])))
  //         .filter(feedEntryAttribute => {
  //           return feedEntryAttribute[0] === 'checked';
  //         })
  //         .filter(feedEntryAttribute => {
  //           return feedEntryAttribute[1] === true;
  //         })
  //         .find(() => true);
  //       })
  //   return theFeeds
  // })

  return (
    <ProcessedPostsFetch>
      <>ProcessedPosts.tsx</>
    </ProcessedPostsFetch>
  )
}


export default ProcessedPosts;
