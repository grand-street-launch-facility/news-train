import React, { FunctionComponent, Suspense} from 'react';
import { Link } from '@material-ui/core';

import { RouteComponentProps } from '@reach/router';
import FeedContentFetch from './FeedContentFetch'
import FeedContentParse from './FeedContentParse'
import Posts from './Posts';
import NavBar from './NavBar'
import BlockUi from 'react-block-ui';

const Home: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  return (

     <Suspense fallback={
        <BlockUi tag="div" blocking={true}/>
     }>
      <FeedContentFetch>
        <FeedContentParse>
          <Posts />
        </FeedContentParse>
      </FeedContentFetch>


      <NavBar>
        <Link href="/settings">settings</Link>
        <Link href="/about">about</Link>
        <Link href="/classifiers">classifiers</Link>
      </NavBar>
     </Suspense>
  );
};

export default Home;
