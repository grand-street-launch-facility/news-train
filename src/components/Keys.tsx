import React, { FunctionComponent } from 'react';
import { RouteComponentProps } from '@reach/router';
import { Box, Divider } from '@material-ui/core';
import KeysAdd from './KeysAdd';
import KeysJSON from './KeysJSON';
import KeysReset from './KeysReset';
import KeyToggle from './KeyToggle';
import KeyDelete from './KeyDelete';
import { useKeys } from '../custom-hooks/useKeys';

const Keys: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  const { keys } = useKeys();
  return (
    <>
      <Box p={1}>
        <KeysAdd />
      </Box>
      <Box p={1} maxWidth={600}>
        {Object.keys(JSON.parse(JSON.stringify(keys))).map(key => {
          return (
            <Box key={key}>
              <Box display="flex">
                <Box width="100%">
                  <KeyToggle text={key} />
                </Box>
                <Box flexShrink={0}>
                  <KeyDelete text={key} />
                </Box>
              </Box>
            </Box>
          );
        })}
      </Box>
      <Box p={1}>
        <KeysReset />
      </Box>
      <Box display="none">
        <Divider />
        <KeysJSON />
      </Box>
      <Box display="none">
        <Divider />
        <pre>{JSON.stringify(Object.keys(JSON.parse(JSON.stringify(keys))), null, 2)}</pre>
      </Box>
    </>
  );
};

export default Keys;
