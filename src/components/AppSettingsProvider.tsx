import React, { FunctionComponent, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSettings } from '../custom-hooks/useSettings';

export const AppSettingsContext = React.createContext({});

const AppSettingsProvider: FunctionComponent = ({ children }) => {
  const { settings } = useSettings();
  const deepCopySettings = JSON.parse(JSON.stringify(settings))
  useEffect(() => {
    // console.log('AppSettingsProvider')
  }, [])
  return (
    <AppSettingsContext.Provider value={deepCopySettings}>
      {children}
    </AppSettingsContext.Provider>
  );
};

AppSettingsProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AppSettingsProvider;
