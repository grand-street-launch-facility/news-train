import { createContext } from 'react';
import createPersistedState from 'use-persisted-state';
const useFeedsState = createPersistedState('feeds');
export type Feed = { categories: string[]; checked: boolean; text: string[] };
const defaultFeeds = {"https://feeds.businessinsider.com/custom/all":{"checked":true,"categories":["business"]},"https://electrek.co/web-stories/feed/":{"checked":true,"categories":["technology"]},"https://www.teslarati.com/feed/":{"checked":true,"categories":["science"]},"https://www.reutersagency.com/feed/":{"checked":false,"categories":["variety"]},"https://en.mercopress.com/rss/":{"checked":true,"categories":["world"]},"https://sputniknews.com/keyword_News_Feed/":{"checked":false,"categories":["world"]},"http://oembed.libsyn.com?item_id=15015401&format=xml":{"checked":true,"categories":[]},"https://consortiumnews.com/feed/":{"checked":false,"categories":["politics"]},"https://scheerpost.com/feed/":{"checked":true,"categories":["politics"]},"http://feeds.feedburner.com/scitechdaily?format=xml":{"checked":true,"categories":["technology"]},"https://cointelegraph.com/rss":{"checked":true,"categories":["bitcoin"]},"https://www.coindesk.com/arc/outboundfeeds/rss/?outputType=xml":{"checked":true,"categories":["bitcoin"]},"http://rss.slashdot.org/Slashdot/slashdotMain":{"checked":false,"categories":["technology"]},"https://sf.streetsblog.org/feed/":{"checked":true,"categories":["local"]},"https://www.themoscowtimes.com/rss/news":{"checked":true,"categories":["world"]},"https://news.bitcoin.com/feed/":{"checked":true,"categories":["bitcoin"]},"https://lifehacker.com/rss":{"categories":["variety"],"checked":false},"https://www.statnews.com/feed":{"checked":false,"categories":["technology"]},"https://theintercept.com/feed/?lang=en":{"checked":true,"categories":["politics"]},"https://www.nytimes.com/svc/collections/v1/publish/https://www.nytimes.com/section/world/rss.xml":{"checked":false,"categories":["world"]},"https://www.nytimes.com/svc/collections/v1/publish/https://www.nytimes.com/section/us/rss.xml":{"checked":false,"categories":["us"]},"https://rt.com/rss":{"checked":false,"categories":["world"]},"https://ft.com/?format=rss":{"checked":true,"categories":["business"]},"https://www.scmp.com/rss/91/feed":{"checked":true,"categories":["world"]},"https://www.scmp.com/rss/5/feed":{"checked":false,"categories":["world"]},"https://oilprice.com/rss/main":{"checked":true,"categories":["world"]},"https://news.google.com/_/rss?hl=en-US&gl=US&ceid=US:en":{"categories":["variety"],"checked":false}}

export const FeedsContext = createContext(defaultFeeds);

export const useFeeds = () => {
  const [feeds, setFeeds] = useFeedsState(() => defaultFeeds);
  return {
    feeds,
    setFeeds,
    factoryReset: () => setFeeds(defaultFeeds),
  };
};
