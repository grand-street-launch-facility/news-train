import { createContext } from 'react';
import createPersistedState from 'use-persisted-state';
const useDispatchersState = createPersistedState('dispatchers');
export type dispatchers = { action: any[]; checked: boolean, keys?: string[]};

const postToBlockstackNoEncryption = (content: string) => console.log('postToBlockstackNoEncryption')
const encryptThenPostToBlockstack = (content: string, keys: string[]) => console.log('encryptToKeys')

const defaultDispatchers = {
  local: {
    'blockstack-narrowcast':
      {
        'checked':false,
        'keys':['lucky-day','dusty-bottoms', 'venus-flytrap', 'ned-nederlander'],
        'action': [encryptThenPostToBlockstack]
      },
      'blockstack-broadcast':
      {
        'checked':false,
        'action': postToBlockstackNoEncryption
      }
    },
    world: {
      'blockstack-narrowcast':
        {
          'checked':false,
          'keys':['lucky-day','dusty-bottoms', 'venus-flytrap', 'ned-nederlander'],
          'action': [encryptThenPostToBlockstack]
        },
        'blockstack-broadcast':
        {
          'checked':false,
          'action': postToBlockstackNoEncryption
        }
      }
}

export const DispatchersContext = createContext(defaultDispatchers);

export const useDispatchers = () => {
  const [dispatchers, setDispatchers] = useDispatchersState(() => defaultDispatchers);
  return {
    dispatchers,
    setDispatchers,
    factoryReset: () => setDispatchers(defaultDispatchers)
  };
};
