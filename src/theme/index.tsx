import { createMuiTheme } from '@material-ui/core/styles';
import {
  // amber,
  // blue,
  blueGrey,
  // brown,
  // cyan,
  // deepOrange,
  // deepPurple,
  // green,
  // grey,
  // indigo,
  // lightBlue,
  // lightGreen,
  // lime,
  // orange,
  // pink,
  // purple,
  // red,
  // teal,
  // yellow,
} from '@material-ui/core/colors';

export default createMuiTheme({
  palette: {
    //type: 'dark',
    primary: blueGrey,
  },
});
